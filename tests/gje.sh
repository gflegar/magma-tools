PRECISIONS="s d"
OUT="-wout -wm -wcsv"
TESTS="-tgje -tgjempw -tlu_cublas -tmatinv_cublas"
BSIZES="16 32"

BSTART=500
BSTEP=$BSTART
BEND=$((20 * $BSTART))

if [ -z "$OUTPATH" ]; then
    echo "OUTPATH environment variable not set - saving to current directory"
    OUTPATH=$(pwd)
fi

if [ ! -z $ARCH ]; then
    ARCH=_$ARCH
fi

MTYPE=RND
if [ ! -z $1 ]; then
    MTYPE=$1
fi

TESTNAME=$(basename $0 .sh)
MATRICES=$(for i in $(seq $BSTART $BSTEP $BEND); do echo %$MTYPE$i; done)

cd sparse/testing

for prec in $PRECISIONS; do
    NAME=${prec}gebjp_setup
    for bs in $BSIZES; do
        CMD=testing_$NAME
        FILE=$OUTPATH/${TESTNAME}_${prec}${ARCH}_${bs}
        echo "Running $CMD with block size $bs, saving to $FILE"
        ./$CMD -o$FILE $OUT $TESTS -b$bs $MATRICES
    done
done

