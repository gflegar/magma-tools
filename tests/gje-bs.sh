PRECISIONS="s d"
OUT="-wout -wm -wcsv"
TESTS="-tgje -tgjempw -tlu_cublas -tmatinv_cublas"
BSIZES=$(seq 1 32)
BATCH=10000

if [ -z "$OUTPATH" ]; then
    echo "OUTPATH environment variable not set - saving to current directory"
    OUTPATH=$(pwd)
fi

if [ ! -z $ARCH ]; then
    ARCH=_$ARCH
fi

MTYPE=RND
if [ ! -z $1 ]; then
    MTYPE=$1
fi

TESTNAME=$(basename $0 .sh)

cd sparse/testing

for prec in $PRECISIONS; do
    NAME=${prec}gebjp_setup
    DATA=$(for bs in $BSIZES; do echo "%$MTYPE$(($bs * $BATCH)):$bs"; done)
    CMD=testing_$NAME
    FILE=$OUTPATH/${TESTNAME}_${prec}${ARCH}
    echo "Running $CMD, saving to $FILE"
    ./$CMD -o$FILE $OUT $TESTS $DATA
done

