PRECISIONS="s d"
OUT="-wout -wm -wcsv"
TESTS="-tm -tbjp-mpw -tbjp-cache"
BSIZES="8 16 32"
MATRICES=$MTXLIST
MSTART=1000
MSTEP=$MSTART
MEND=$((10 * $MSTART))

if [ -z "$OUTPATH" ]; then
    echo "OUTPATH environment variable not set - saving to current directory"
    OUTPATH=$(pwd)
fi

if [ ! -z $ARCH ]; then
    ARCH=_$ARCH
fi

if [ -z "$MATRICES" ]; then
    MTYPE=$1
    if [ -z $MTYPE ]; then
        MTYPE=RND
    fi
    MATRICES=$(for i in $(seq $MSTART $MSTEP $MEND); do echo %$MTYPE$i; done)
fi

TESTNAME=$(basename $0 .sh)

cd sparse/testing

for prec in $PRECISIONS; do
    NAME=${prec}gebjp_setup
    for bs in $BSIZES; do
        CMD=testing_$NAME
        FILE=$OUTPATH/${TESTNAME}_${prec}${ARCH}_${bs}
        echo "Running $CMD with block size $bs, saving to $FILE"
        ./$CMD -o$FILE $OUT $TESTS -b$bs $MATRICES
    done
done

