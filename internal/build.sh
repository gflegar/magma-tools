mkdir -p $BUILDPATH
if [ -z $BUILDTYPE ]; then
    BUILDTYPE="default"
fi

if [ ! -z $1 ]; then
    export BUILDTYPE=$1
    $TOOLSPATH/magmatools build
    exit
fi

echo "Building MAGMA, configuration $BUILDTYPE (source location: $MAGMAPATH)"

mkdir -p $BUILDPATH/$BUILDTYPE

rsync -av $MAGMAPATH/ $BUILDPATH/$BUILDTYPE --exclude=.hg
cp $TOOLSPATH/builds/make.inc.$BUILDTYPE \
    $BUILDPATH/$BUILDTYPE/make.inc 2>/dev/null

pushd $BUILDPATH/$BUILDTYPE >/dev/null

make lib -j4
cd sparse
make -j4

popd >/dev/null

