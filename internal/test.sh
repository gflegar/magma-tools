TESTNAME=$1
shift

OUTDIR=$TIMESTAMP
RESDIR=$TOOLSPATH/results/$BUILDTYPE

export OUTPATH=$RESDIR/$OUTDIR
mkdir -p $OUTPATH

echo "Running test $TESTNAME..."
pushd $BUILDPATH/$BUILDTYPE >/dev/null
bash $TOOLSPATH/tests/$TESTNAME.sh "$@"
popd >/dev/null
echo "DONE!"

echo -n "Compressing results... "
pushd $RESDIR >/dev/null
tar -czf $OUTDIR.tar.gz $OUTDIR && rm -r $OUTDIR
popd >/dev/null
echo "Results written to $RESDIR/$OUTDIR.tar.gz"

