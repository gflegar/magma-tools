LISTPATH=$TOOLSPATH/matrices
DATAPATH=$LISTPATH/data

UPSTREAM=http://www.cise.ufl.edu/research/sparse
EXT=tar.gz

echo "Downloading index file"
curl -Lo $DATAPATH/index.txt $UPSTREAM/mat/UF_Listing.txt

mkdir -p $DATAPATH

PATTERN=$(cat $1)

LIST=$(grep "${PATTERN// /\\|}" $DATAPATH/index.txt)

echo "Found $(wc -w <<<$LIST) matches:"

if [ -z $2 ]; then
    echo "$LIST";
else
    echo "$LIST" | tee $LISTPATH/$2.txt
fi

