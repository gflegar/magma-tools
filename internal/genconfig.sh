source $TOOLSPATH/internal/config.cfg 2>/dev/null && exit 0

while [ -z $MAGMAPATH ]; do
    echo -n "Path to MAGMA's source code: "
    read pathstring
    MAGMAPATH=$(readlink -e $(envsubst <<<$pathstring))
    if [ -z $MAGMAPATH ]; then
        echo -n "Directory $pathstring doesn't exist. "
    fi
done

while [ -z $BUILDPATH ]; do
    echo -n "Build path: "
    read pathstring
    BUILDPATH=$(readlink -m $(envsubst <<<$pathstring))
    if [ -z $BUILDPATH ]; then
        echo -n "$pathstring is not a valid directory. "
    fi
done

cat << EOF >$TOOLSPATH/internal/config.cfg
MAGMAPATH=$MAGMAPATH
BUILDPATH=$BUILDPATH
EOF

cat $TOOLSPATH/internal/config.cfg

