REMOTE=$1
shift

RCMD="ssh $REMOTE"
RCPY="rsync -av --exclude=.git --exclude=.hg"
if [ -z $REMOTEPATH ]; then
    REMOTEPATH="magma"
fi
RHOME=$($RCMD "echo \$HOME")
REMOTEPATH=$RHOME/$REMOTEPATH

echo "Creating directory structure on $REMOTE:$REMOTEPATH"
$RCMD "mkdir -p $REMOTEPATH/{tools,source,build}"

echo "Copying magma-tools to $REMOTE:$REMOTEPATH/tools"
$RCPY $TOOLSPATH/ $REMOTE:$REMOTEPATH/tools \
    --exclude=config.cfg --exclude=matrices/data --exclude=results

echo "Copying MAGMA to $REMOTE:$REMOTEPATH/source"
$RCPY $MAGMAPATH/ $REMOTE:$REMOTEPATH/source

echo "Setting remote config"
$RCMD "tee $REMOTEPATH/tools/internal/config.cfg >/dev/null" << EOF
MAGMAPATH=$REMOTEPATH/source
BUILDPATH=$REMOTEPATH/build
EOF

CMD="env BUILDTYPE=$REMOTE bash $REMOTEPATH/tools/magmatools $@"
# echo $CMD
$RCMD $CMD

