NODES=$1
SCRIPTNAME=/tmp/magma-tools.sh
shift
cat << EOF >$SCRIPTNAME
env BUILDTYPE=$BUILDTYPE bash $TOOLSPATH/magmatools $@
EOF

qsub -lnodes=$NODES $SCRIPTNAME

