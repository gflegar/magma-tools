LISTPATH=$TOOLSPATH/matrices
DATAPATH=$LISTPATH/data

UPSTREAM=https://www.cise.ufl.edu/research/sparse
ARCH_EXT=tar.gz

mkdir -p $DATAPATH

echo "Downloading index file"
curl -Lo $DATAPATH/index.txt $UPSTREAM/mat/UF_Listing.txt

# PATTERN="$@"
# LIST=$(grep ${PATTERN// /\\|}  matrices/index.txt)

# echo "Found $(wc -w <<<$LIST) matches:"
# echo "$LIST"

LIST=$(cat $LISTPATH/$1.txt)

if [ -z "$FETCHMTX_FORMATS" ]; then
    FETCHMTX_FORMATS=MM
fi

for format in $FETCHMTX_FORMATS; do
    case "$format" in
        mat)
            EXT=mat
            MAT_EXT=mat
            ;;
        RB)
            EXT=$ARCH_EXT
            MAT_EXT=rb
            ;;
        MM)
            EXT=$ARCH_EXT
            MAT_EXT=mtx
            ;;
        *)
            echo "Unrecognized format \"$format\""
            continue;
    esac
    for entry in $LIST; do
        GROUP=$(dirname $entry)
        NAME=$(basename $entry)
        if [ -e $DATAPATH/$GROUP/$NAME.$MAT_EXT ]; then
            echo "$entry ($format) already downloaded"
        else
            echo "Downloading $entry ($format)"
            mkdir -p $DATAPATH/$GROUP
            curl -Lo $DATAPATH/$GROUP/$NAME.$EXT\
                $UPSTREAM/$format/$GROUP/$NAME.$EXT
            if [ "$EXT" != "$MAT_EXT" ]; then
                echo "Extracting $entry"
                tar -xzf $DATAPATH/$GROUP/$NAME.$EXT --strip-components=1\
                    -C $DATAPATH/$GROUP/
                rm $DATAPATH/$GROUP/$NAME.$EXT
            fi
        fi
    done
done

