LISTPATH=$TOOLSPATH/matrices
DATAPATH=$LISTPATH/data
export MTXLIST=$(for mtx in $(cat $LISTPATH/$1.txt); do
    echo $DATAPATH/$mtx.mtx
done)
shift
bash $TOOLSPATH/magmatools "$@"

