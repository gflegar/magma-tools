MAGMA-tools
===========

A collection of scripts which facilitate the development and testing of
MAGMA(-sparse). Look at the script description section (and the `internal`
directory) to get an idea of what functionalities are here.

Installation
------------

Clone the repository to a suitable location. The first time you run
the `magmatools` script, it will ask you for the location of the MAGMA's
sources and the desired build directory.
Some functionalities (like fetchmtx) can be used without MAGMA. In case you
only want to use these functionalities, you can provide any valid directory
name when prompted (though the source directory must also exist in the system).

If you want to change the build or the source directory later, modify the
`internal/config.cfg` file.

Script description
------------------

### fetchmtx

Usage:

```bash

./magmatools fetchmtx <matrix-list>

```

Download a list of matrices &lt;matrix-list&gt; in matrix market format from
the SuitSparse matrix collection.

The list to download is read from `matrices/<matrix-list>.txt`
which is expected to contain one matrix identifier per line, formatted as
`<group-name>/<matrix-name>`. The downloaded matrices are saved to
`matrices/data/<group-name>/<matrix-name>.mtx`. If the file of that name
already exists (i.e. the matrix was already downloaded), the script will not
try to download id again.

This command depends on the `curl` and `tar` utilities to download and extract
the data.

The storage format(s) in which the matrices should be downloaded can be
specified by setting the `FETCHMTX_FORMATS` environment variable to the list of
desired formats. Supported formats are:
*   MM (matrix market format, .mtx extension),
*   RB (Rutherford/Boeing format, .rb extension),
*   mat (MATLAB format, .mat extension);

If `FETCHMTX_FORMATS` is not set, it defaults to "MM".

Examples:

Creating and fetching a list containing "Alemdar" ("Alemdar" group)
and "pkustk01" ("Chen" group).

*   Create a file `matrices/my-list.txt` with the following content:
```
Alemdar/Alemdar
Chen/pkustk01
```
*   Call `./magmatools fetchmtx my-list`.
*   The matrices should now be located at `matrices/data/Alemdar/Alemdar.mtx`.
    and `matrices/data/Chen/pkustk01.mtx`.

To get the above matrices in all three storage formats, one could use the
following command:

```bash
env FETCHMTX_FORMATS="MM RB mat" ./magmatools fetchmtx my-list
```

